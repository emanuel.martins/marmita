import json
import requests


def enviar_mensagem(mensagem: str, webhook_url):
    payload = {
        "text": mensagem
    }
    headers = {
        "Content-Type": "application/json"
    }
    response = requests.post(webhook_url, data=json.dumps(payload), headers=headers)
    if response.status_code == 200:
        print("Mensagem enviada com sucesso!")
    else:
        print("Ocorreu um erro ao enviar a mensagem. Código de status:", response.status_code)
