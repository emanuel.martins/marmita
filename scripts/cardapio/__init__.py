from datetime import datetime

from .models.opcao_cadapio import OpcaoCardapio

def criar_cardapio() -> str:
    nome_restaurante = input("Digite o nome do restaurante: ")
    numero_opcoes = obter_numero_opcoes()
    opcoes_cardapio = definir_cardapio_cada_opcao(numero_opcoes)
    taxa_entrega = obter_taxa_entrega()
    taxa_servico = obter_taxa_servico()
    cardapio = construir_comanda(nome_restaurante, opcoes_cardapio, taxa_entrega, taxa_servico)
    print(cardapio)
    return cardapio

def obter_numero_opcoes() -> int:
    return int(input('Quantas opções há no cardápio? '))

def definir_cardapio_cada_opcao(numero_opcoes: int) -> list[OpcaoCardapio]:
    opcoes_cardapio: list[OpcaoCardapio] = []
    for item in range(numero_opcoes):
        opcao = input(f"\nDescreva a opção {item + 1} do menu: \n")
        preco_pequena = float(input(f"Descreva o preço da opção {item + 1} pequena do menu: R$ ").replace(",", "."))
        preco_media = float(input(f"Descreva o preço da opção {item + 1} média do menu: R$ ").replace(",", "."))
        preco_grande = float(input(f"Descreva o preço da opção {item + 1} grande do menu: R$ ").replace(",", "."))
        opcoes_cardapio.append(OpcaoCardapio(opcao, preco_pequena, preco_media, preco_grande))
    return opcoes_cardapio

def obter_taxa_entrega() -> float:
    valor_padrao = 0.00
    possui_taxa_entrega = input("\nPossui taxa de entrega? (S - Sim)\n")
    if possui_taxa_entrega.upper() == "S":
        valor_padrao = float(input("Digite o valor da taxa: R$ ").replace(",", "."))
        return valor_padrao
    return valor_padrao

def obter_taxa_servico() -> float:
    valor_padrao = 1.00
    possui_taxa_servico = input("\nPossui taxa de serviço? (S - Sim) (N - Não)\n")
    if possui_taxa_servico.upper() == "S":
        valor_padrao = float(input("Digite o valor da taxa: R$ ").replace(",", "."))
        return valor_padrao
    elif possui_taxa_servico.upper() == "N":
        return 0.00
    return valor_padrao

def construir_comanda(nome_restaurante: str, opcoes_cardapio: list[OpcaoCardapio], taxa_entrega: float, taxa_servico: float) -> str:
    cardapio = ""
    for index, item in enumerate(opcoes_cardapio):
        cardapio += f"Opção {index + 1}: {item.opcao} \n (Pequena R$ {item.preco_pequena:.2f} - Média R$ {item.preco_media:.2f} - Grande R$ {item.preco_grande:.2f})\n\n"
    return f"""
=========================================
Restaurante: {nome_restaurante}

📅 Data: {datetime.now().strftime("%d/%m/%Y")}

📑 MENU

{cardapio}
=========================================
ENTREGA:       R$ {taxa_entrega:.2f}
SERVIÇO:       R$ {taxa_servico:.2f}
=========================================
PIX: 46999207432
=========================================
"""
