class OpcaoCardapio:
    opcao: str
    preco_pequena: float
    preco_media: float
    preco_grande: float

    def __init__(self, opcao: str, preco_pequena: float, preco_media: float, preco_grande: float):
        self.opcao = opcao
        self.preco_pequena = preco_pequena
        self.preco_media = preco_media
        self.preco_grande = preco_grande

    def to_dict(self):
        return {
            "opcao": self.opcao,
            "preco_pequena": self.preco_pequena,
            "preco_media": self.preco_media,
            "preco_grande": self.preco_grande
        }
