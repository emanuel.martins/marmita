from os import getenv, system
from dotenv import load_dotenv

from scripts import cardapio as funcoes_cardapio
from scripts.core import google_webhook


load_dotenv()
is_app_running = True

while is_app_running:
    try:
        cardapio = funcoes_cardapio.criar_cardapio()
        google_webhook.enviar_mensagem(cardapio, getenv("IDEVS_CHAT"))        
        is_app_running = False

    except ValueError:
        system("clear")
        print("Entrada inválida. Por favor, digite um número inteiro.")
    except TypeError as err:
        system("clear")
        print("Não foi possível definir as opções do cardápio, motivo:\n", err)
